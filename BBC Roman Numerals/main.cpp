#include <iostream>
#include <array>
#include <functional>

////////////////////////////////////////////////////////
// RomanNumeralGenerator
////////////////////////////////////////////////////////

struct RomanNumeralGenerator {
  static std::string generate(unsigned int number);
};

std::string RomanNumeralGenerator::generate(unsigned int number) {
  // Check for valid range.
  if (1 > number || number > 3999)
    return std::string();

  // Reserve string.
  std::string output;
  output.reserve(16);

  // Declare numerals
  static const std::array<std::string[9], 4> numerals {
    {
      { "I","II","III","IV","V","VI","VII","VIII","IX" },
      { "X","XX","XXX","XL","L","LX","LXX","LXXX","XC" },
      { "C","CC","CCC","CD","D","DC","DCC","DCCC","CM" },
      { "M","MM","MMM","MV","","","","","" }
    }
  };

  for (const auto& numeral : numerals) {
    // Get remainder of end digit, pushing the numeral to the front.
    if (auto value = number % 10)
      output.insert(0, numeral[value - 1]);

    // Pop the end digit.
    number /= 10;
  }

  return output;
}

////////////////////////////////////////////////////////
// TestModule
////////////////////////////////////////////////////////

class TestModule {
public:
  void TestCase(const std::function<bool()>&, const char*);

  inline auto getTestCount() const { return m_testCount; }
  inline auto getPassCount() const { return m_passCount; }
  inline auto getPassed() const { return m_testCount == m_passCount; }
private:
  unsigned int m_testCount{ 0 };
  unsigned int m_passCount{ 0 };
};

void TestModule::TestCase(const std::function<bool()>& unitTest, const char* message) {
  ++m_testCount;
  try {
    if (unitTest()) {
      ++m_passCount;
      return;
    }
  }
  catch (std::exception&) {}
  std::cerr << "Test " << m_testCount << " failed. [" << message << "]" << std::endl;
}

////////////////////////////////////////////////////////
// main
////////////////////////////////////////////////////////

static bool UnitTests() {
  TestModule tm;

  std::cout << "Starting unit tests..." << std::endl;

  // Use lambda to store the expected outcome in a temporary variable.
  auto GeneratorTest = [&](unsigned int number, std::string outcome, const char* message) {
    tm.TestCase([&]() {
      return outcome.compare(RomanNumeralGenerator::generate(number)) == 0;
    }, message);
  };

#define CHECK_DIGIT(NUM, ROM) GeneratorTest(NUM, ROM, "Check single digit : "#NUM)

  CHECK_DIGIT(1, "I");
  CHECK_DIGIT(2, "II");
  CHECK_DIGIT(3, "III");
  CHECK_DIGIT(4, "IV");
  CHECK_DIGIT(5, "V");
  CHECK_DIGIT(6, "VI");
  CHECK_DIGIT(7, "VII");
  CHECK_DIGIT(8, "VIII");
  CHECK_DIGIT(9, "IX");

  CHECK_DIGIT(10, "X");
  CHECK_DIGIT(20, "XX");
  CHECK_DIGIT(30, "XXX");
  CHECK_DIGIT(40, "XL");
  CHECK_DIGIT(50, "L");
  CHECK_DIGIT(60, "LX");
  CHECK_DIGIT(70, "LXX");
  CHECK_DIGIT(80, "LXXX");
  CHECK_DIGIT(90, "XC");

  CHECK_DIGIT(100, "C");
  CHECK_DIGIT(200, "CC");
  CHECK_DIGIT(300, "CCC");
  CHECK_DIGIT(400, "CD");
  CHECK_DIGIT(500, "D");
  CHECK_DIGIT(600, "DC");
  CHECK_DIGIT(700, "DCC");
  CHECK_DIGIT(800, "DCCC");
  CHECK_DIGIT(900, "CM");

  CHECK_DIGIT(1000, "M");
  CHECK_DIGIT(2000, "MM");
  CHECK_DIGIT(3000, "MMM");

#undef CHECK_DIGIT

  // Checks out of bounds values.
  GeneratorTest(0, "", "Exceeds lower bounds");
  GeneratorTest(4000, "", "Exceeds upper bounds");
  GeneratorTest(99999, "", "Further exceeds upper bounds");

  // Checks concatination of digits.
  GeneratorTest(1111, "MCXI", "Basic combination");
  GeneratorTest(3888, "MMMDCCCLXXXVIII", "Long combination");
  GeneratorTest(1001, "MI", "Skipping combination");

  // Checks bizarre casting outputs.
  GeneratorTest((unsigned int)'a', "XCVII", "Check ascii cast : 'a'(97)");
  GeneratorTest((unsigned int)'A', "LXV", "Check ascii cast : 'A'(65)");
  GeneratorTest((unsigned int)nullptr, "", "Check nullptr cast");

  bool passed = tm.getPassed();

  if (passed)
    std::cout << "\033[1;32m" << "All tests passed! " << tm.getPassCount() << " out of " << tm.getTestCount() << "\033[0m" << std::endl;  // Prints in green.
  else
    std::cerr << "\033[1;31m" << "Tests failed! "     << tm.getPassCount() << " out of " << tm.getTestCount() << "\033[0m" << std::endl;  // Prints in red.

  // Pause for user input, as to view results of tests.
  std::cout << "Press any key to continue..." << std::endl;
  std::cin.ignore();

  return passed;
}

void ManualTests() {
  std::cout << "Starting unit tests..." << std::endl;

  unsigned int input;
  do
  {
    std::cout << "Enter value to convert (0 to espace): ";
    std::cin >> input;

    // Check input state.
    if (!std::cin.good()) {
      std::cin.clear();
      std::cin.ignore();
      std::cout << "Invalid user input" << std::endl;
      continue;
    }

    auto numeral = RomanNumeralGenerator::generate(input);
    if (numeral.empty())
      std::cout << input << " failed to convert" << std::endl;
    else
      std::cout << input << " converts to " << numeral.c_str() << std::endl;
  } while (input);
}


int main(int argc, char *argv[]) {
#ifdef _WIN32
  // Enable console colours in windows.
  system("color");
#endif

  std::cout << "Roman Numeral Generator" << std::endl;
  std::cout << "Enter value to continue:" << std::endl;
  std::cout << "  1) Unit Tests" << std::endl;
  std::cout << "  2) Manual Tests" << std::endl;

  int input;
  std::cin >> input;
  std::cin.clear();
  std::cin.ignore();

  switch (input) {
  case 1:
    return UnitTests() ? 0 : 1;
  case 2:
    ManualTests();
    return 0;
  default:
    std::cerr << "Invlid user input" << std::endl;
    return 1;
  }
  
  return -1;
}